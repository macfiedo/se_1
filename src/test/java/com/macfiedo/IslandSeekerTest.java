package com.macfiedo;

import java.io.ByteArrayInputStream;
import java.nio.channels.SeekableByteChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IslandSeekerTest {

  private IslandSeeker seeker;

  private static final Map<String, Integer> mapDefinitions;
  static {
    Map<String, Integer> tmpMap = new HashMap<>();
    tmpMap.put("111\n111\n111\n\n", 1);
    tmpMap.put("101\n101\n101\n\n", 2);
    tmpMap.put("101\n000\n101\n\n", 4);
    tmpMap.put("0 0 0 0 0 0 0 0 0\n" +
              "0 1 0 0 0 0 0 0 0\n" +
              "1 1 1 0 0 0 1 0 0\n" +
              "1 1 0 0 0 1 1 1 0\n" +
              "0 0 0 0 0 1 1 0 0\n" +
              "0 0 1 0 0 0 0 0 0\n" +
              "1 1 0 0 0 0 0 0 0\n" +
              "0 0 0 0 0 1 1 0 0\n\n", 4);
    tmpMap.put("\n\n", 0);

    mapDefinitions = tmpMap;
  }

  @Test
  public void searchForIslands() {
    for(Map.Entry entry : mapDefinitions.entrySet()) {
      ByteArrayInputStream in = new ByteArrayInputStream(((String)entry.getKey()).getBytes());
      Scanner scanner = new Scanner(in);
      seeker = new IslandSeeker();
      int result = seeker.searchForIslands(scanner);
      assertEquals(entry.getValue(), result);
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void searchForIslandsWIthForbiddenCharacters() {
    ByteArrayInputStream in = new ByteArrayInputStream(("abcd\n\n").getBytes());
    Scanner scanner = new Scanner(in);
    seeker = new IslandSeeker();
    seeker.searchForIslands(scanner);
  }

}