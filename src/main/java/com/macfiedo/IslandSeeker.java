package com.macfiedo;

import java.util.*;

public class IslandSeeker {

  private int numberOfIslands;
  private List<List<Integer>> islandsInLastRow;

  public IslandSeeker(){
    islandsInLastRow = null;
    numberOfIslands = 0;
  }

  public boolean analyzeRow(String row){
    List<List<Integer>> islandsInCurrentRow = new ArrayList<>();
    List<Integer> island = new ArrayList<>();
    boolean isFinish = false;
    char[] processedRow = processRow(row);
    if(processedRow.length == 0){
      isFinish = true;
    }
    for(int i=0;i<processedRow.length;i++){
      if('1' == processedRow[i]) {
        island.add(i);
        if(i == processedRow.length-1){
          if(!island.isEmpty()){
            islandsInCurrentRow.add(new ArrayList<>(island));
            island.clear();
          }
        }
      }else if('0' == processedRow[i]) {
        if (!island.isEmpty()) {
          islandsInCurrentRow.add(new ArrayList<>(island));
          island.clear();
        }
      }else{
        throw new IllegalArgumentException("Array contains forbidden characters!");
      }
    }
    countIslands(islandsInCurrentRow);
    return isFinish;
  }

  private void countIslands(List<List<Integer>> islandsInCurrentRow){
    if(islandsInLastRow == null) {
      numberOfIslands = islandsInCurrentRow.size();
    }else{
      boolean isNewIsland = true;
      for(List<Integer> islandFromCurrentRow: islandsInCurrentRow){
        for(List<Integer> islandFromLastRow: islandsInLastRow){
          if(!intersection(islandFromCurrentRow, islandFromLastRow).isEmpty()){
            isNewIsland = false;
            break;
          }
        }
        if(isNewIsland){
          numberOfIslands++;
        }
      }
    }
    islandsInLastRow = islandsInCurrentRow;
  }

  private char[] processRow(String row){
    return row.replace(" ", "").toCharArray();
  }

  public int getNumberOfIslands(){
    return numberOfIslands;
  }

  private List<Integer> intersection(List<Integer> list1, List<Integer> list2){
    List<Integer> result = new ArrayList<Integer>(list1);
    result.retainAll(list2);
    return result;
  }

  public int searchForIslands(Scanner scanner){
    String row;
    while (!(row = scanner.nextLine()).isEmpty()) {
      if(analyzeRow(row)){
        break;
      }
    }
    return getNumberOfIslands();
  }
}
