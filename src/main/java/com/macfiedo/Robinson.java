package com.macfiedo;

import com.sun.javafx.runtime.SystemProperties;
import java.util.Scanner;

public class Robinson {

  public static void main(String args[]){
    searchForIslands();
  }

  private static void searchForIslands(){
    Scanner sc = new Scanner(System.in);
    sc.useDelimiter("\n");
    while(true) {
      IslandSeeker seeker = new IslandSeeker();
      System.out.println(seeker.searchForIslands(sc));
    }
  }


}
